<!--{template header}-->
<!--{eval $expertlist=$this->fromcache('expertlist');}-->
<!--专家列表-->

<!--{if $expertlist}-->

<div class="au_expert_list" style="padding:7px;margin:0px;">
     <div class="ws_expert_title">
         <i class="fa fa-user"></i>专家推荐
     </div>
        <div class="swiper-container">
<div class="swiper-wrapper"><!--{loop $expertlist $expert}-->
<div class="swiper-slide" data-swiper-autoplay="2000">
<div class="row">
<div class="col col-md-8">
<div class="au_expert_info"><!--头像-->
<div class="au_expert_info_avatar"><a
	href="{url user/space/$expert['uid']}"><img
	src="{$expert['avatar']}" class="" /></a></div>
<!--个人名称-->
<p class="au_expert_info_author"><a
	href="{url user/space/$expert['uid']}">{$expert['username']} {if $expert['author_has_vertify']!=false}<i class="fa fa-vimeo {if $expert['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  " data-toggle="tooltip" data-placement="right" title="" {if $expert['author_has_vertify'][0]==0}data-original-title="个人认证" {else}data-original-title="企业认证" {/if} ></i>{/if} </a></p>
<!--个人简介-->
<p class="au_expert_info_text">{$expert['signature']}</p>

<div class="au_expert_btn_roup">{if $expert['hasfollower']==1}
<div id="attenttouser_{$expert['uid']}"
	onclick="attentto_user($expert['uid'])"
	class="au_btn_guanzhu button_followed following"><i class="fa fa-check"></i> 已关注
</div>
{else}
<div id="attenttouser_{$expert['uid']}"
	onclick="attentto_user($expert['uid'])" class="au_btn_guanzhu button_attention follow">
+关注</div>
{/if}

<div class="au_btn_fufei" data-placement="right" data-toggle="tooltip"
	data-original-title="付费{$expert['mypay']}元咨询" {if $user['uid']==0}
	onclick="window.location.href='{url user/login}'"
	{else} onclick="window.location.href='{url question/add/$expert[uid]}'"{/if}><i
	class="fa fa-twitch"></i>付费咨询</div>
</div>
<!--{if $expert['category']}--> <!--擅长领域-->
<p class="au_expert_shanchang">擅长领域: <!--{loop $expert['category'] $category}-->
<span> <a href="{url category/view/$category['cid']}">{$category['categoryname']}</a></span>
<!--{/loop}--></p>
<!--{/if}--></div>
</div>
</div>
</div>

<!--{/loop}--></div>

</div>

    <!-- 如果需要分页器 -->
   <div class="myswiper-pagination">  <div class="swiper-pagination"></div></div>

</div>
<!--{/if}-->
<!--热门主题-->
<!--{template sider_hottopic}-->
     <!--{eval $topdatalist=$this->fromcache('topdata');}-->
     
     {if  $topdatalist}
<!--顶置内容-->
<div class="au_side_box" style="padding:0px;margin:0px;">

    <div class="au_box_title ws_mynewquestion" style="    padding: 7px;">

        <div>
            <i class="fa fa-file-text-o lv"></i>顶置内容

        </div>

    </div>
 
    <div class="au_side_box_content">
 <div class="stream-list question-stream xm-tag tag-nosolve">

<!--{loop $topdatalist  $topdata}-->
      <section class="stream-list__item">

                <div class="qa-rank"><div class="answers answered solved ml10 mr10">
               {$topdata['answers']}<small>顶置</small></div></div>     
           
                
                
          
                   <div class="summary">
            <ul class="author list-inline">
                                           
                                                <li class="authorinfo">
                            
                          <a href="{url user/space/$topdata['model']['authorid']}">{$topdata['model']['author']} {if $topdata['model']['author_has_vertify']!=false}<i class="fa fa-vimeo {if $topdata['model']['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  "  ></i>{/if}</a>
                      
                     
                       
                        <span class="split"></span>
                        <a href="{$topdata['url']}">{$question['format_time']}</a>
                                    </li>
            </ul>
            <h2 class="title"><a href="{$topdata['url']}">{$topdata['title']}</a></h2>

                   
                           
                                            </div>
    </section>
    <!--{/loop}-->
  
   
      
      </div>
      </div>
    </div>
    
    {/if}
<!--最新问题-->
<div class="au_side_box" style="padding:0px;margin:0px;">

  
    <div class="au_side_box_content">
        <!--导航-->
        <ul class="tab-head au_tabs">
            <li class="tab-head-item au_tab current" data-tag="tag-nosolve"><a>全部</a></li>
             <li class="tab-head-item au_tab" data-tag="tag-solvelist"><a>已解决</a></li>
            <li class="tab-head-item au_tab" data-tag="tag-score"><a>积分悬赏</a></li>
             {if 1==$setting['openwxpay'] }
            <li class="tab-head-item au_tab" data-tag="tag-shangjinscore"><a>现金悬赏</a></li>
              {/if} 
               {if $setting['mobile_localyuyin']==1}
            <li class="tab-head-item au_tab" data-tag="tag-hasvoice"><a>语音回答</a></li>
            {/if} 

        </ul>

      <!--最新问题列表部分-->
      
      <div class="stream-list question-stream xm-tag tag-nosolve">
      <!--{eval $vnosolvelist=$this->fromcache('nosolvelist');}--> <!--{loop $vnosolvelist $index $question}-->
      
      <section class="stream-list__item">
       {if $question['status']==2}
                <div class="qa-rank"><div class="answers answered solved ml10 mr10">
                {$question['answers']}<small>解决</small></div></div>     
                {else}
                {if $question['answers']>0}
                <div class="qa-rank"><div class="answers answered ml10 mr10">
                $question['answers']<small>回答</small></div>
                </div>
                   {else}
                   <div class="qa-rank"><div class="answers ml10 mr10">
                0<small>回答</small></div></div>
                {/if}
                
                
                {/if}
                   <div class="summary">
            <ul class="author list-inline">
                                           
                                                <li class="authorinfo">
                                          {if $question['hidden']==1}
                                            匿名用户
                      
                       {else} 
                              <a href="{url user/space/$question['authorid']}">
                          {$question['author']}{if $question['author_has_vertify']!=false}<i class="fa fa-vimeo {if $question['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  " ></i>{/if}
                          </a>
                      
                         {/if} 
                       
                        <span class="split"></span>
                        <a href="{url question/view/$question['id']}">{$question['format_time']}</a>
                                    </li>
            </ul>
            <h2 class="title"><a href="{url question/view/$question['id']}">{$question['title']}</a></h2>
 <!--{if $question['tags']}-->
           <ul class="taglist--inline ib">
<!--{loop $question['tags'] $tag}-->
<li class="tagPopup authorinfo">
                        <a class="tag" href="{url tags/view/$tag['tagalias']}" >
                                                       {$tag['tagname']}
                        </a>
                    </li>
                    

                           
                <!--{/loop}-->
                 </ul>
                <!--{else}--><!--{/if}-->
                
              
                                   
                           
                                            </div>
    </section>
    <!--{/loop}-->
  
      <div class="text-center">
     
      <ul class="pagination"><li class="next"><a rel="next" href="{url new/default}">查看更多</a></li></ul>
       </div>
      
      </div>

<!--积分问题列表部分-->

 <div class="stream-list question-stream xm-tag tag-score hide">
  <!--{eval $vnosolvelist=$this->fromcache('rewardlist');}--> <!--{loop $vnosolvelist $index $question}-->

      <section class="stream-list__item">
       {if $question['status']==2}
                <div class="qa-rank"><div class="answers answered solved ml10 mr10">
                {$question['answers']}<small>解决</small></div></div>     
                {else}
                {if $question['answers']>0}
                <div class="qa-rank"><div class="answers answered ml10 mr10">
                $question['answers']<small>回答</small></div>
                </div>
                   {else}
                   <div class="qa-rank"><div class="answers ml10 mr10">
                0<small>回答</small></div></div>
                {/if}
                
                
                {/if}
                   <div class="summary">
            <ul class="author list-inline">
                                           
                                                <li class="authorinfo">
                                          {if $question['hidden']==1}
                                            匿名用户
                      
                       {else} 
                          <a href="{url user/space/$question['authorid']}">{$question['author']} {if $question['author_has_vertify']!=false}<i class="fa fa-vimeo {if $question['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  "  ></i>{/if}</a>
                      
                         {/if} 
                       
                        <span class="split"></span>
                        <a href="{url question/view/$question['id']}">{$question['format_time']}</a>
                                    </li>
            </ul>
            <h2 class="title"><a href="{url question/view/$question['id']}">{$question['title']}<span class="icon_price" ><i
	class="fa fa-database"></i>$question['price']</span></a></h2>
 <!--{if $question['tags']}-->
           <ul class="taglist--inline ib">
<!--{loop $question['tags'] $tag}-->
<li class="tagPopup authorinfo">
                        <a class="tag" href="{url tags/view/$tag['tagalias']}" >
                                                       {$tag['tagname']}
                        </a>
                    </li>
                    

                           
                <!--{/loop}-->
                 </ul>
                <!--{else}--><!--{/if}-->
                
              
                                   
                           
                                            </div>
    </section>
    <!--{/loop}-->
  
      <div class="text-center">
     
      <ul class="pagination"><li class="next"><a rel="next" href="{url new/default/1}">查看更多</a></li></ul>
       </div>
      
      </div>


<!--现金问题列表部分-->

 <div class="stream-list question-stream xm-tag tag-shangjinscore hide">
<!--{eval $vnosolvelist=$this->fromcache('shangjinlist');}--> <!--{loop $vnosolvelist $index $question}-->

      <section class="stream-list__item">
       {if $question['status']==2}
                <div class="qa-rank"><div class="answers answered solved ml10 mr10">
                {$question['answers']}<small>解决</small></div></div>     
                {else}
                {if $question['answers']>0}
                <div class="qa-rank"><div class="answers answered ml10 mr10">
                $question['answers']<small>回答</small></div>
                </div>
                   {else}
                   <div class="qa-rank"><div class="answers ml10 mr10">
                0<small>回答</small></div></div>
                {/if}
                
                
                {/if}
                   <div class="summary">
            <ul class="author list-inline">
                                           
                                                <li class="authorinfo">
                                          {if $question['hidden']==1}
                                            匿名用户
                      
                       {else} 
                          <a href="{url user/space/$question['authorid']}">{$question['author']} {if $question['author_has_vertify']!=false}<i class="fa fa-vimeo {if $question['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  "  ></i>{/if}</a>
                      
                         {/if} 
                       
                        <span class="split"></span>
                        <a href="{url question/view/$question['id']}">{$question['format_time']}</a>
                                    </li>
            </ul>
            <h2 class="title"><a href="{url question/view/$question['id']}">{$question['title']}<span class="icon_rmb" ><img
	src="{SITE_URL}static/css/aozhou/dist/images/rmb.png" />悬赏$question['shangjin']元</span></a></h2>
 <!--{if $question['tags']}-->
           <ul class="taglist--inline ib">
<!--{loop $question['tags'] $tag}-->
<li class="tagPopup authorinfo">
                        <a class="tag" href="{url tags/view/$tag['tagalias']}" >
                                                       {$tag['tagname']}
                        </a>
                    </li>
                    

                           
                <!--{/loop}-->
                 </ul>
                <!--{else}--><!--{/if}-->
                
              
                                   
                           
                                            </div>
    </section>
    <!--{/loop}-->
  
      <div class="text-center">
     
      <ul class="pagination"><li class="next"><a rel="next" href="{url new/default/2}">查看更多</a></li></ul>
       </div>
      
      </div>
  
<!--语音问题列表部分-->

<div class="stream-list question-stream xm-tag tag-hasvoice hide">
<!--{eval $vnosolvelist=$this->fromcache('yuyinlist');}--> <!--{loop $vnosolvelist $index $question}-->

      <section class="stream-list__item">
       {if $question['status']==2}
                <div class="qa-rank"><div class="answers answered solved ml10 mr10">
                {$question['answers']}<small>解决</small></div></div>     
                {else}
                {if $question['answers']>0}
                <div class="qa-rank"><div class="answers answered ml10 mr10">
                $question['answers']<small>回答</small></div>
                </div>
                   {else}
                   <div class="qa-rank"><div class="answers ml10 mr10">
                0<small>回答</small></div></div>
                {/if}
                
                
                {/if}
                   <div class="summary">
            <ul class="author list-inline">
                                           
                                                <li class="authorinfo">
                                          {if $question['hidden']==1}
                                            匿名用户
                      
                       {else} 
                          <a href="{url user/space/$question['authorid']}">{$question['author']} {if $question['author_has_vertify']!=false}<i class="fa fa-vimeo {if $question['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  "  ></i>{/if}</a>
                      
                         {/if} 
                       
                        <span class="split"></span>
                        <a href="{url question/view/$question['id']}">{$question['format_time']}</a>
                                    </li>
            </ul>
            <h2 class="title"><a href="{url question/view/$question['id']}">{$question['title']} <span
	class="au_q_yuyin"><i class="fa fa-microphone"></i></span></a></h2>
 <!--{if $question['tags']}-->
           <ul class="taglist--inline ib">
<!--{loop $question['tags'] $tag}-->
<li class="tagPopup authorinfo">
                        <a class="tag" href="{url tags/view/$tag['tagalias']}" >
                                                       {$tag['tagname']}
                        </a>
                    </li>
                    

                           
                <!--{/loop}-->
                 </ul>
                <!--{else}--><!--{/if}-->
                
              
                                   
                           
                                            </div>
    </section>
    <!--{/loop}-->
  
      <div class="text-center">
     
      <ul class="pagination"><li class="next"><a rel="next" href="{url new/default/3}">查看更多</a></li></ul>
       </div>
      
      </div>
      

<!--已解决问题列表部分-->


<div class="stream-list question-stream xm-tag tag-solvelist hide">
<!--{eval $vnosolvelist=$this->fromcache('solvelist');}--> <!--{loop $vnosolvelist $index $question}-->

      <section class="stream-list__item">
       {if $question['status']==2}
                <div class="qa-rank"><div class="answers answered solved ml10 mr10">
                {$question['answers']}<small>解决</small></div></div>     
                {else}
                {if $question['answers']>0}
                <div class="qa-rank"><div class="answers answered ml10 mr10">
                $question['answers']<small>回答</small></div>
                </div>
                   {else}
                   <div class="qa-rank"><div class="answers ml10 mr10">
                0<small>回答</small></div></div>
                {/if}
                
                
                {/if}
                   <div class="summary">
            <ul class="author list-inline">
                                           
                                                <li class="authorinfo">
                                          {if $question['hidden']==1}
                                            匿名用户
                      
                       {else} 
                          <a href="{url user/space/$question['authorid']}">{$question['author']} {if $question['author_has_vertify']!=false}<i class="fa fa-vimeo {if $question['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  "  ></i>{/if}</a>
                      
                         {/if} 
                       
                        <span class="split"></span>
                        <a href="{url question/view/$question['id']}">{$question['format_time']}</a>
                                    </li>
            </ul>
            <h2 class="title"><a href="{url question/view/$question['id']}">{$question['title']} </a></h2>
 <!--{if $question['tags']}-->
           <ul class="taglist--inline ib">
<!--{loop $question['tags'] $tag}-->
<li class="tagPopup authorinfo">
                        <a class="tag" href="{url tags/view/$tag['tagalias']}" >
                                                       {$tag['tagname']}
                        </a>
                    </li>
                    

                           
                <!--{/loop}-->
                 </ul>
                <!--{else}--><!--{/if}-->
                
              
                                   
                           
                                            </div>
    </section>
    <!--{/loop}-->
  
      <div class="text-center">
     
      <ul class="pagination"><li class="next"><a rel="next" href="{url new/default/4}">查看更多</a></li></ul>
       </div>
      
      </div>
 
</div>

 
  
  <div class="au_side_box" style="padding:0px;margin:0px;">

    <div class="au_box_title ws_mynewquestion" style="    padding: 7px;">

        <div>
            <i class="fa fa-file-text-o lv"></i>一周热点

        </div>

    </div>
    
     
      <div class="stream-list question-stream ">
       <!--{eval $attentionlist = $this->fromcache("attentionlist");}-->
                                          <!--{loop $attentionlist $index $question}-->
      <section class="stream-list__item">
       {if $question['status']==2}
                <div class="qa-rank"><div class="answers answered solved ml10 mr10">
                {$question['answers']}<small>解决</small></div></div>     
                {else}
                {if $question['answers']>0}
                <div class="qa-rank"><div class="answers answered ml10 mr10">
                $question['answers']<small>回答</small></div>
                </div>
                   {else}
                   <div class="qa-rank"><div class="answers ml10 mr10">
                0<small>回答</small></div></div>
                {/if}
                
                
                {/if}
                   <div class="summary">
            <ul class="author list-inline">
                                           
                                                <li class="authorinfo">
                                          {if $question['hidden']==1}
                                            匿名用户
                      
                       {else} 
                              <a href="{url user/space/$question['authorid']}">
                          {$question['author']}{if $question['author_has_vertify']!=false}<i class="fa fa-vimeo {if $question['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  " ></i>{/if}
                          </a>
                      
                         {/if} 
                       
                        <span class="split"></span>
                        <a href="{url question/view/$question['id']}">{$question['format_time']}</a>
                                    </li>
            </ul>
            <h2 class="title"><a href="{url question/view/$question['id']}">{$question['title']}</a></h2>
 <!--{if $question['tags']}-->
           <ul class="taglist--inline ib">
<!--{loop $question['tags'] $tag}-->
<li class="tagPopup authorinfo">
                        <a class="tag" href="{url tags/view/$tag['tagalias']}" >
                                                       {$tag['tagname']}
                        </a>
                    </li>
                    

                           
                <!--{/loop}-->
                 </ul>
                <!--{else}--><!--{/if}-->
                
              
                                   
                           
                                            </div>
    </section>
    <!--{/loop}-->
  
      
      </div>
 
    </div>
    
     <div class="ui-col side " style="float: none;padding:7px;">
     <div class="widget-box pt0 " style="border:none;">
                        <h2 class="h4 widget-box__title" style="margin-bottom: 5px">热门标签</h2>
                        <ul class="taglist--inline multi">
                                <!--{eval $hosttaglist = $this->fromcache("hosttaglist");}-->
                                 <!--{loop $hosttaglist $index $rtag}-->
                                
                                                            <li class="tagPopup">
                                    <a class="tag" href="{url tags/view/$rtag['tagalias']}" >
                                                                          {if $rtag['tagimage']}  <img src="$rtag['tagimage']">{/if}
                                                                        {$rtag['tagname']}</a>
                                </li>
                                                <!--{/loop}-->          
                                                    </ul>
                    </div>
  </div>
</section>

{if $signPackage!=null}
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"> </script>

<script>

  wx.config({
      debug: false,
      appId: '{$signPackage["appId"]}',
      timestamp: {$signPackage["timestamp"]},
      nonceStr: '{$signPackage["nonceStr"]}',
      signature: '{$signPackage["signature"]}',
      jsApiList: [
                  'checkJsApi',
        'onMenuShareTimeline',
        'onMenuShareAppMessage',
        'onMenuShareQQ',
        'onMenuShareWeibo',
        'hideMenuItems'
        
      ]
  });

</script>

<script>


var _topictitle="{$setting['site_name']}";
var imgurl="{$setting['share_index_logo']}"; 	
var topicdescription="{$setting['seo_index_description']}";
var topiclink="{url index}";
wx.ready(function () {
    wx.checkJsApi({
	      jsApiList: [
	       
	        'onMenuShareTimeline',
	        'onMenuShareAppMessage',
	        'onMenuShareQQ',
	        'onMenuShareWeibo'
	      ],
	      success: function (res) {
	       // alert(JSON.stringify(res));
	      }
	    });
    wx.hideMenuItems({
	    menuList: ['menuItem:copyUrl','menuItem:openWithQQBrowser','menuItem:openWithSafari','menuItem:originPage','menuItem:share:email']
	});
    wx.onMenuShareAppMessage({
	      title:_topictitle ,
	      desc:topicdescription ,
	      link:topiclink,
	      imgUrl: imgurl,
	      trigger: function (res) {
	      //  alert('用户点击发送给朋友');
	      },
	      success: function (res) {
	    
	
	      },
	      cancel: function (res) {
	    	  el2=$.tips({
	   	            content:'取消分享',
	   	            stayTime:1000,
	   	            type:"info"
	   	        });
	       // alert('已取消');
	      },
	      fail: function (res) {
	       // alert(JSON.stringify(res));
	      }
	    });
    wx.onMenuShareTimeline({
	      title:_topictitle,
	      link:topiclink,
	      imgUrl: imgurl,
	      trigger: function (res) {
	       // alert('用户点击分享到朋友圈');
	      },
	      success: function (res) {
	    	
	      //  alert('已分享');
	      },
	      cancel: function (res) {
	    	  el2=$.tips({
	   	            content:'取消分享',
	   	            stayTime:1000,
	   	            type:"info"
	   	        });
	      //  alert('已取消');
	      },
	      fail: function (res) {
	       // alert(JSON.stringify(res));
	      }
	    });
    wx.onMenuShareQZone({
   	      title: _topictitle,
	      desc:'来自微信分享' ,
	      link:topiclink,
	      imgUrl: imgurl,
	      trigger: function (res) {
		       // alert('用户点击分享到朋友圈');
		      },
		      success: function (res) {
		    	  el2=$.tips({
		   	            content:'已分享',
		   	            stayTime:1000,
		   	            type:"success"
		   	        });
		      //  alert('已分享');
		      },
		      cancel: function (res) {
		    	  el2=$.tips({
		   	            content:'取消分享',
		   	            stayTime:1000,
		   	            type:"info"
		   	        });
		      //  alert('已取消');
		      },
		      fail: function (res) {
		       // alert(JSON.stringify(res));
		      }
   });
    wx.onMenuShareQQ({
	      title: _topictitle,
	      desc:'来自微信分享' ,
	      link:topiclink,
	      imgUrl: imgurl,
	      trigger: function (res) {
	       // alert('用户点击分享到QQ');
	      },
	      complete: function (res) {
	      //  alert(JSON.stringify(res));
	      },
	      success: function (res) {
	       // alert('已分享');
	      },
	      cancel: function (res) {
	    	   el2=$.tips({
	  	            content:'取消分享',
	  	            stayTime:1000,
	  	            type:"info"
	  	        });
	      //  alert('已取消');
	      },
	      fail: function (res) {
	       // alert(JSON.stringify(res));
	      }
	    });

    wx.onMenuShareWeibo({
    	 title:_topictitle,
    	 desc:'来自微信分享' ,
	      link:topiclink,
	      imgUrl: imgurl,
	      trigger: function (res) {
	       // alert('用户点击分享到微博');
	      },
	      complete: function (res) {
	       // alert(JSON.stringify(res));
	      },
	      success: function (res) {
	       // alert('已分享');
	      },
	      cancel: function (res) {
	       // alert('已取消');
	      },
	      fail: function (res) {
	      //  alert(JSON.stringify(res));
	      }
	    });
});
</script>
{/if}
<script src="{SITE_URL}static/js/jquery-1.11.3.min.js"></script>
<script>$.noConflict();</script>
<script>
    var swiper = new Swiper('.swiper-container', {
    
       
        slidesPerView: 2,
        paginationClickable: true,
        spaceBetween: 10,
        // 如果需要分页器
        pagination: {
            el: '.swiper-pagination',
        }
    });

    jQuery(".au_tabs .au_tab").click(function(){

    	jQuery(".xm-tag").addClass("hide");
    	jQuery(".au_tabs .au_tab").removeClass("current");
    	jQuery(this).addClass("current");
    	jQuery("."+$(this).attr("data-tag")).removeClass("hide");
    })
</script>
<!--{template footer}-->
