<!--{template meta}-->
<link rel="stylesheet" media="all" href="{SITE_URL}static/css/common/commtag.css" />
 <div class="m_search userspaceheader">

    <div class="weui-flex">
      
        <div class="weui-flex__item"> <span class="ws_h_title">

        {if isset($touser)}

您正在向【{$touser['username']}】提问：

{else}
        提问
        {/if}
        </span></div>
        
    </div>
</div>
<style>
.userspaceheader {
    background: #fff;
    color: #333;
    box-shadow: 1px 1px 1px 1px #ebebeb;
}

.m_search {
    font-size: .16rem;
    height: .4rem;
    line-height: .4rem;
   background: #009A61;
    z-index: 6;
    text-align: center;
    color: #fff;
    position: fixed;
    top: 0px;
    left: 0px;
    width: 100%;
}
.moreinfo{
	color:#009A61;
padding-left:.16rem;
    cursor: pointer;
text-align: left;
margin-top:.1rem;
}
.moreinfoitem{
	display:none;
}
.mycny{
    color: #ff8f00;
    font-size: 17px;
    position: absolute;
    top: .13rem;
    left: .16rem;
}
#qxianjin{
	padding-left:.16rem;
}
#login_form{
margin-top:.03rem;
}
</style>
<section class="ui-container mar-t-01" style="margin-top: 50px">



<div class="ui-form ui-border-t">
        <form id="login_form" onsubmit="return false;">
        <input type="hidden"  id="forward" name="return_url" value="{$forward}">

            <div class="ui-form-item ui-form-item-pure ui-border-b">
                <input name="title" id="qtitle" value="{$word}" maxlength="50" type="text" placeholder="标题简短，言简意赅，问号结尾">
                <a href="#" class="ui-icon-close"></a>
            </div>
            <div class=" ui-form-item-pure ui-border-b">
           <!--{template editor}-->
               <style>
.editor_container {
    box-shadow:none;
}
            </style>
            </div>



             <div class="ui-form-item ui-form-item-r ui-border-b">
            <label>分类</label>
            <div class="ui-select">
                              <select  name="srchcategory" id="srchcategory">

                              {$catetree}
                              </select>
            </div>
        </div>


                 <div class="ui-form-item ui-form-item-pure ui-border-b hideapp">
                <i class="fa fa-cny mycny"></i>
                <input name="xianjin" id="qxianjin" value=""  type="text" placeholder="现金悬赏金额，没有请先充值">
                <a href="#" class="ui-icon-close"></a>
            </div>
            <div class="hideapp" style="color:red;padding-left:15px;">当前可用余额【{echo $user['jine']/100;}元】</div>
            <!--{if $iswxbrower==null&&$setting['code_ask']&&$user['credit1']<$setting['jingyan']}-->
  <div class="ui-form-item ui-form-item-pure ui-border-b">

 {template code}
  </div>
   <!--{/if}-->
   <div class="moreinfo">增加更多信息<i class="fa fa-angle-down"></i></div>
   <div class="ui-form-item ui-form-item-r ui-border-b moreinfoitem hideapp">
            <label>积分悬赏</label>
            <div class="ui-select">
                <select name="givescore" id="scorelist">
                   <option selected="selected" value="0">0</option><option value="3">3</option><option value="5">5</option><option value="10">10</option><option value="15">15</option><option value="30">30</option><option value="50">50</option><option value="80">80</option><option value="100">100</option>
                </select>
            </div>
        </div>

        
         <!--{if $user['uid']}-->
            <div class="ui-form-item ui-form-item-switch ui-border-b moreinfoitem">
                <p>
                    匿名提问
                </p>
                <label class="ui-switch">
                   <input type="checkbox" name="hidanswer" id="hidanswer" value="1" />
                </label>
            </div>
             <!--{/if}-->
              
                   <div class="ui-form-item ui-form-item-switch ui-border-b ">
                <p>
                                      <a id="showadreeitem">同意隐私条款[查看]</a>
                </p>
                <label class="ui-switch">
                   <input type="checkbox"  id="agreeitem" value="1" checked/>
                </label>
            </div>
             <div class=" moreinfoitem" style="height: auto;line-height:auto;">
                   <div class="form-group" style="padding:0 15px;">

          <div class=" dongtai ">
          <div class="tags">
      
          </div>
            <input type="text" style="border: solid 1px #009A61;" autocomplete="off"  data-toggle="tooltip" data-placement="bottom" title="" placeholder="检索标签，最多添加5个,添加标签更容易被回答" data-original-title="检索标签，最多添加5个" name="topic_tag" value=""  class="txt_taginput" >
            <i class="fa fa-search" style="color:#009A61"></i>
           <div class="tagsearch">
        
          
           </div>
            
          </div>
      
</div>
            </div>
            <div class="ui-btn-wrap">
             <input type="hidden" name="cid" id="cid"/>
                    <input type="hidden" name="cid1" id="cid1" value="0"/>
                    <input type="hidden" name="cid2" id="cid2" value="0"/>
                    <input type="hidden" name="cid3" id="cid3" value="0"/>
                    <input type="hidden" value="{$askfromuid}" id="askfromuid" name="askfromuid" />

                <input type="hidden" id="tokenkey" name="tokenkey" value='{$_SESSION["addquestiontoken"]}'/>
                 <!--{if $touser}-->
                         <!--{if $touser['mypay']>0}-->
                            <button id="asksubmit"  class="ui-btn-lg ui-btn-primary">
                                         付费{$touser['mypay']}元提问
                </button>

                          <!--{else}-->
                              <!-- 若按钮不可点击则添加 disabled 类 -->
                <button id="asksubmit"  class="ui-btn-lg ui-btn-primary">
                                            提交问题
                </button>
                            <!--{/if}-->

               <!--{else}-->
                            <!-- 若按钮不可点击则添加 disabled 类 -->
                <button id="asksubmit"  class="ui-btn-lg ui-btn-primary">
                                            提交问题
                </button>
                            <!--{/if}-->

            </div>
             <div class="ui-btn-wrap">
            
             </div>
        </form>
    </div>
    <div class="modal fade" id="agreemodel" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="padding-top: 50px;">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">提问同款说明</h4>
      </div>

        <div class="modal-body">
                    <div style="height: 450px;overflow:scroll;">
                        <p>&nbsp; &nbsp; &nbsp; &nbsp;当您提问时，表示您已经同意遵守本规章。&nbsp;</p><p>欢迎您加入本站点参加交流和讨论，本站点为公共论坛，为维护网上公共秩序和社会稳定，请您自觉遵守以下条款：&nbsp;</p><p><br></p><p>一、不得利用本站危害国家安全、泄露国家秘密，不得侵犯国家社会集体的和公民的合法权益，不得利用本站制作、复制和传播下列信息：</p><p>　 （一）煽动抗拒、破坏宪法和法律、行政法规实施的；</p><p>　（二）煽动颠覆国家政权，推翻社会主义制度的；</p><p>　（三）煽动分裂国家、破坏国家统一的；</p><p>　（四）煽动民族仇恨、民族歧视，破坏民族团结的；</p><p>　（五）捏造或者歪曲事实，散布谣言，扰乱社会秩序的；</p><p>　（六）宣扬封建迷信、淫秽、色情、赌博、暴力、凶杀、恐怖、教唆犯罪的；</p><p>　（七）公然侮辱他人或者捏造事实诽谤他人的，或者进行其他恶意攻击的；</p><p>　（八）损害国家机关信誉的；</p><p>　（九）其他违反宪法和法律行政法规的；</p><p>　（十）进行商业广告行为的。</p><p><br></p><p>二、互相尊重，对自己的言论和行为负责。</p><p>三、禁止在申请用户时使用相关本站的词汇，或是带有侮辱、毁谤、造谣类的或是有其含义的各种语言进行注册用户，否则我们会将其删除。</p><p>四、禁止以任何方式对本站进行各种破坏行为。</p><p>五、如果您有违反国家相关法律法规的行为，本站概不负责，您的登录论坛信息均被记录无疑，必要时，我们会向相关的国家管理部门提供此类信息。</p>
                    </div>
                </div>


    </div>
  </div>
</div>
</section>

<script>
$(".txt_taginput").on(" input propertychange",function(){
	 var _txtval=$(this).val();
	 if(_txtval.length>1){
	
		 //检索标签信息
		 var _data={tagname:_txtval};
		 var _url="{url tags/ajaxsearch}";
		 function success(result){
			 console.log(result)
			 if(result.code==200){
				 console.log(_txtval)
				  $(".tagsearch").html("");
				for(var i=0;i<result.taglist.length;i++){
			
					 var _msg=result.taglist[i].tagname
					 
			           $(".tagsearch").append('<div class="tagitem" tagid="'+result.taglist[i].id+'">'+_msg+'</div>');
				}
				$(".tagsearch").show();
				$(".tagsearch .tagitem").click(function(){
					var _tagname=$.trim($(this).html());
					var _tagid=$.trim($(this).attr("tagid"));
					if(gettagsnum()>=5){
						alert("标签最多添加5个");
						return false;
					}
					if(checktag(_tagname)){
						alert("标签已存在");
						return false;
					}
					$(".dongtai .tags").append('<div class="tag"><span tagid="'+_tagid+'">'+_tagname+"</span><i class='fa fa-close'></i></div>");
					$(".dongtai .tags .tag  .fa-close").click(function(){
						$(this).parent().remove();
					});
					$(".tagsearch").html("");
					$(".tagsearch").hide();
					$(".txt_taginput").val("");
					});
		        
			 }
			 
		 }
		 ajaxpost(_url,_data,success);
	 }else{
			$(".tagsearch").html("");
			$(".tagsearch").hide();
	 }
})
	function checktag(_tagname){
		var tagrepeat=false;
		$(".dongtai .tags .tag span").each(function(index,item){
			var _tagnametmp=$.trim($(this).html());
			if(_tagnametmp==_tagname){
				tagrepeat=true;
			}
		})
		return tagrepeat;
	}
	function gettaglist(){
		var taglist='';
		$(".dongtai .tags .tag span").each(function(index,item){
			var _tagnametmp=$.trim($(this).attr("tagid"));
			taglist=taglist+_tagnametmp+",";
			
		})
		taglist=taglist.substring(0,taglist.length-1);
	
		return taglist;
	}
	function gettagsnum(){
    return $(".dongtai .tags .tag").length;
	}
	$(".tagsearch .tagitem").click(function(){
		var _tagname=$.trim($(this).html());
		if(gettagsnum()>=5){
			alert("标签最多添加5个");
			return false;
		}
		if(checktag(_tagname)){
			alert("标签已存在");
			return false;
		}
		$(".dongtai .tags").append('<div class="tag"><span>'+_tagname+"</span><i class='fa fa-close'></i></div>");
		$(".dongtai .tags .tag  .fa-close").click(function(){
			$(this).parent().remove();
		});
		$(".tagsearch").html("");
		$(".tagsearch").hide();
		$(".txt_taginput").val("");
		});
	$(".dongtai .tags .tag  .fa-close").click(function(){
		$(this).parent().remove();
	});
$(".close").click(function(){
	$('#agreemodel').hide();
});
$("#showadreeitem").click(function(){
	$('#agreemodel').show();
}
);
$(".moreinfo").click(function(){
	$(".moreinfoitem").show();
	$(".moreinfo").hide();
})
	getcat();

function getcat(){

	var sv=$("#srchcategory").val();

	 $.ajax({
	        //提交数据的类型 POST GET
	        type:"POST",
	        //提交的网址
	        url:"{url question/ajaxgetcat}",
	        //提交的数据
	        data:{category:sv},
	        //返回数据的格式
	        datatype: "json",//"xml", "html", "script", "json", "jsonp", "text".
	      //在请求之前调用的函数
	        beforeSend:function(){

	        },
	        //成功返回之后调用的函数
	        success:function(data){

	        	var data=eval("("+data+")");

	        	  if(data.message=='ok'){
	        		  $("#cid").val(data.cid);
	        		  $("#cid1").val(data.cid1);
	        		  $("#cid2").val(data.cid2);
	        		  $("#cid3").val(data.cid3);

	        	  }else{
	        		  el2=$.tips({
          	            content:'分类不存在，估计是缓存引起',
          	            stayTime:1000,
          	            type:"info"
          	        });
	        	  }
	        }   ,

	        //调用执行后调用的函数
	        complete: function(XMLHttpRequest, textStatus){

	        },

	        //调用出错执行的函数
	        error: function(){

	            //请求出错处理
	        }
	    });
}
$("#srchcategory").change(function(){
	getcat();

})
$("#asksubmit").tap(function(){
	tijiao();
	return false;
})
var submit=false;
function tijiao(){
	
 	if(gettagsnum()>5){
        alert("最多添加5个标签");
        return false;
   	}
   	 var _tagstr=gettaglist();
	 var qtitle = $("#qtitle").val();
    if (bytes($.trim(qtitle)) < 8 || bytes($.trim(qtitle)) > 100) {


        el2=$.tips({
	            content:'问题标题长度不得少于4个字，不能超过50字！',
	            stayTime:1500,
	            type:"info"
	        });
        $("#qtitle").focus();
        return false;
    }

    {if $user['uid']}
    //检查财富值是否够用
    var offerscore = 0;
    var selectsocre = $("#givescore").val();
	 if ($('#hidanswer').attr('checked')) {
        offerscore += 10;
    }
    offerscore += parseInt(selectsocre);
    if (offerscore > $user['credit2']) {

      el2=$.tips({
          content:'你的财富值不够!',
          stayTime:1000,
          type:"info"
      });
            return false;
    }
    {/if}

 
    	// var eidtor_content= editor.getContent();
	 // 获取编辑器区域
        var _txt = editor.wang_txt;
     // 获取 html
        var eidtor_content =  _txt.html();
	 var _hidanswer=0;
	 if ($('#hidanswer').attr('checked')) {
		 _hidanswer=1;
	 }

	   var money_reg = /\d{1,4}/;
       var _money = $("#qxianjin").val();
       if('' == _money){
       	_money=0;
       }
	  <!--{if $setting['code_ask']}-->
	  var data={
			  title:$("#qtitle").val(),
			  description:eidtor_content,
			  cid:$("#cid").val(),
			  cid1:$("#cid1").val(),
			  cid2:$("#cid2").val(),
			  cid3:$("#cid3").val(),
			  givescore:$("#scorelist").val(),
			  hidanswer:_hidanswer,
			  askfromuid:$("#askfromuid").val(),
			  tokenkey:$("#tokenkey").val(),
			  jine:_money,
			  tags:_tagstr,
 			code:$("#code").val()
 	}
	    <!--{else}-->
		var data={
				  title:$("#qtitle").val(),
   			  description:eidtor_content,
   			  cid:$("#cid").val(),
   			  cid1:$("#cid1").val(),
   			  cid2:$("#cid2").val(),
   			  cid3:$("#cid3").val(),
   			  givescore:$("#scorelist").val(),
   			tokenkey:$("#tokenkey").val(),
   			  hidanswer:_hidanswer,
   			jine:_money,
   		 tags:_tagstr,
   			  askfromuid:$("#askfromuid").val()



   	}
	     <!--{/if}-->
		     
if(submit==true){
	
	return false;
}

submit=true;
	    	   var el='';
	$.ajax({
       //提交数据的类型 POST GET
       type:"POST",
       //提交的网址
       url:"{url question/ajaxadd}",
       //提交的数据
       data:data,
       async: false,
       //返回数据的格式
       datatype: "json",//"xml", "html", "script", "json", "jsonp", "text".
       //在请求之前调用的函数
       beforeSend:function(){
    	  
    	   el=$.loading({
    	        content:'加载中...',
    	    })
       },
       //成功返回之后调用的函数
       success:function(data){
    	   el.loading("hide");

       	var data=eval("("+data+")");
          if(data.message=='ok'){
              $("#asksubmit").attr("disabled",true);
        	  submit=true;
       	   var tmpmsg='提问成功!';
       	   if(data.sh=='1'){
       		   tmpmsg='问题发布成功！为了确保问答的质量，我们会对您的提问内容进行审核。请耐心等待......';
       	   }

           el2=$.tips({
               content:tmpmsg,
               stayTime:1500,
               type:"info"
           });
       	   setTimeout(function(){

                window.location.href=data.url;
              },1500);
          }else{
        	  submit=false;
       	   el2=$.tips({
               content:data.message,
               stayTime:1500,
               type:"info"
           });
          }


       }   ,
       //调用执行后调用的函数
       complete: function(XMLHttpRequest, textStatus){
    	   
    	   el.loading("hide");
    	   return false;
       },
       //调用出错执行的函数
       error: function(){
           //请求出错处理
       }
    });
	return false;
}
</script>
<!--{template footer}-->