<section class="ui-container search-panel">
  <section id="searchbar">


<form action="{url question/search}" onsubmit="return checksearch()" class="am-form am-form-inline"  id="searchForm" name="searchform"   method="post">
                <div  class="ui-searchbar-wrap ui-border-b">

                    <div class="ui-searchbar ui-border-radius">
                        <i class="ui-icon-search"></i>
                        <div class="ui-searchbar-text">输入关键词即可</div>
                        <div class="ui-searchbar-input">
                        <input  onpropertychange="handle();" oninput="handle();" tabindex="1" name="word" id="search-kw" value="{$word}"  type="text" placeholder="输入关键词即可" autocapitalize="off">
                        </div>
                        <i class="ui-icon-close"></i>
                    </div>
                    <button type="submit" id="search_btn" class="ui-searchbar-cancel">取消</button>

                </div>

                <script type="text/javascript">
                Array.prototype.distinct = function(){
                    var arr = this,
                        result = [],
                        i,
                        j,
                        len = arr.length;
                    for(i = 0; i < len; i++){
                        for(j = i + 1; j < len; j++){
                            if(arr[i] === arr[j]){
                                j = ++i;
                            }
                        }
                        result.push(arr[i]);
                    }
                    return result;
                }
                Array.prototype.indexOf = function(val) {
                    for (var i = 0; i < this.length; i++) {
                        if (this[i] == val) return i;
                    }
                    return -1;
                };
                Array.prototype.remove = function(val) {
                    var index = this.indexOf(val);
                    if (index > -1) {
                        this.splice(index, 1);
                    }
                };
                var _myarr=new Array();
                loadword();
                function addword(_word){
                    if(_word==""||_word==null||_word=="undefined"){
return false;
                    }
                    var _tmpmyarr=window.localStorage.getItem("hotword");

                    if(_tmpmyarr){

                        _myarr=JSON.parse(_tmpmyarr);
                    }
                    _myarr.push($.trim(_word));
                  
                    _myarr= _myarr.distinct();
                    window.localStorage.setItem("hotword",JSON.stringify(_myarr));
                    loadword();
                }
                function delword(_word){
                    var _tmpmyarr=window.localStorage.getItem("hotword");
                    if(_tmpmyarr){
                        _myarr=JSON.parse(_tmpmyarr);
                    }
                    _myarr.remove($.trim(_word));
                    _myarr= _myarr.distinct();
                    console.log(_myarr)
                    window.localStorage.clear();
                    window.localStorage.setItem("hotword",JSON.stringify(_myarr));
                    loadword();
                }
               function searchword(_word){
                 window.location.href="{url question/search}?word="+_word;
               }
                function loadword(){
                    var _tmpmyarr=window.localStorage.getItem("hotword");
                    if(_tmpmyarr){
                       
                        _myarr=JSON.parse(_tmpmyarr);
                        console.log(_myarr);
                        $(".search_history .m_search_history_word").remove();
                        for(var i=0;i<_myarr.length;i++){
                            var _w=$.trim(_myarr[i]);
                            
                            if(_w.length>7){
                            	_w=_w.substring(0,7)+"...";
                            }
                            var str='    <div class="weui-flex m_search_history_word"> <div> <i class="icon_clock"></i> </div> <div class="weui-flex__item word" onclick="searchword('+"'"+_w+"'"+')">'+_w+' </div> <div> <i class="icon_close"></i></div> </div>';
                     $(".search_history").append(str);
                     $(".search_history .icon_close").click(function(){
                         var _word=$(this).parent().parent().find('.word').html();
                         delword($.trim(_word));

                     })
                        }
                    }else{
                    	 $(".search_history .m_search_history_word").remove();
$(".trashword").hide();
                    }

                }
                    $('.ui-searchbar').tap(function(){
                        $('.ui-searchbar-wrap').addClass('focus');
                        $('.ui-searchbar-input input').focus();
                    });

                    function checksearch(){
                    	
                   
                    	  var _word=$.trim( $("#search-kw").val());
                          if(_word==''){
                              return false;
                          }
                          addword($.trim(_word))
                    	
                    	
                    	tijiao();
                    }
                    document.getElementById("search_btn").addEventListener("click",cancel,false);
                    //当状态改变的时候执行的函数
                    function handle()
                    {



                        if( $("#search-kw").val()!=''){
                            $("#search_btn").html("检索");
                            document.getElementById("search_btn").removeEventListener("click",cancel,false);


                        }else{
                            $("#search_btn").html("取消");

                            document.getElementById("search_btn").addEventListener("click",cancel,false);
                        }


                    }
                    function tijiao(){


                        document.searchform.action ="{url question/search}";
                        document.searchform.submit();


                    }
                    function cancel(){

                    	$('.ui-searchbar-wrap').removeClass('focus');
                    }
                </script>

 </form>
    </section>