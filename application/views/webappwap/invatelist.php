<!--{template meta}-->
 <div class="m_search userspaceheader">

    <div class="weui-flex hotcat">
      
        <div class="weui-flex__item  <!--{if $regular=='user/invatelist'}-->current<!--{/if}--> "> <span class="ws_h_title"><a href="{url user/invatelist}">我的邀请</a></span></div>
         <div class="weui-flex__item  <!--{if $regular=='user/invateme'}-->current<!--{/if}-->"> <span class="ws_h_title"><a href="{url user/invateme}">邀请回答</a></span></div>
       
    </div>
</div>
<section class="ui-container">


   
    <section class="user-content-list">
    
                          <ul class="ui-list ui-list-one ui-list-link ui-border-tb">
                 {if $followerlist==null}
                
               
    <p class="user-p-tip ui-txt-warning">  你还没有邀请任何人注册。</p>

                 {/if}
                    <!--{loop $followerlist $index $follower}-->
    <li class="ui-border-t" onclick="window.location.href='{url user/space/$follower['uid']}'">
    
        <div class="ui-list-thumb">
            <span style="background-image:url({$follower['avatar']})"></span>
        </div>
        <div class="ui-list-info">
            <h4 class="ui-nowrap">{$follower['username']}</h4>
            <div class="ui-txt-info">
             <!--{if (1 == $follower['gender'])}--> 男 
            
             <!--{else}-->
             女
            <!--{/if}-->
            </div>
        </div>
    </li>
  <!--{/loop}-->    
</ul>
          <div class="pages" >{$departstr}</div>   
    </section>
</section>


<!--{template footer}-->