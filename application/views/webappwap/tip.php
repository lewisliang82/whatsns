<!--{template meta}-->
<style>
    .ui-notice *{
        font-size: .16rem;;
    }
.ui-notice>i:before {
    font-family: iconfont!important;
    font-size: 32px;
    line-height: 44px;
    font-style: normal;
    -webkit-font-smoothing: antialiased;
    -webkit-text-stroke-width: .2px;
    display: block;
    color: rgba(0,0,0,.5);
    content: "";
    font-size: 100px;
    line-height: 100px;
    color: #970d0c;
}
</style>

 <div class="m_search">

    <div class="weui-flex">
        <div><i  onclick="window.location.href='{url index}'" class="fa fa-home"></i></div>
        <div class="weui-flex__item"> <span class="ws_h_title">提示</span></div>
        
    </div>
</div>
<section class="ui-notice">
    <i style="color:#ff973c"></i>
    <p>{$message}</p>
 
    
     <!--{if $message=='登陆成功!'||trim($message)=='成功退出!'||trim($message)=='问题已经被删除!'}-->
     
       <script type="text/javascript">
       setTimeout(function(){
    	   window.location.href="{SITE_URL}";
       },2000);
       
     
       </script>
     <!--{/if}-->
    <div class="ui-notice-btn">
     <!--{if $redirect == 'BACK'}-->
      <button onclick="history.go(-1);" class="ui-btn-primary ui-btn-lg" >返回</button>
        <!--{elseif $redirect!='STOP'}-->
       
      <p style="color:#777;"> <span id="seconds">3</span>秒后自动跳转。
          </p>
            <script type="text/javascript">
            var seconds = 3;
            var inter=window.setInterval(function() {
                seconds--;
                if (seconds == 1) {
                    clearInterval(inter);
                   window.location = "$redirect";
                }
                $("#seconds").html(seconds);
            }, 1000);
        </script>
        
           <!--{/if}-->
       
    </div>
</section>
