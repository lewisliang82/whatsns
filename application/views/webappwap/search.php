<!--{template header}-->
    <style>
        body{
            background: #f1f5f8;
        }
    </style>


    <!--导航提示-->
    <div class="weui-flex ws_s_au_brif">
        <span class="weui-flex__item current"><a href="{url question/search/$word}">问题</a></span>

        <span class="weui-flex__item"><a href="{url topic/search}?word={$word}">文章</a></span>
        <span class="weui-flex__item"><a href="{url user/search}?word={$word}">用户</a></span>
        <span class="weui-flex__item"><a href="{url category/search}?word={$word}">话题</a></span>

    </div>
 <!--列表部分-->
                    <div class="au_resultitems au_searchlist">
                      <!--{if $questionlist}-->
                       
<div class="qlists">
 <!--{loop $questionlist $index $question}-->
    <!--多图文-->
<div class="qlist">
<div class="title weui-flex">
    <div>
    {if $question['hidden']==1}
        <img src="{SITE_URL}static/css/default/avatar.gif">
        {else}
        <img src="{$question['avatar']}">
        {/if}
    </div>
    <div class="weui-flex__item">
       {if $question['hidden']==1}
         <span class="author">匿名用户 </span> 
       
          {else}
          <span class="author">{$question['author']}   {if $question['author_has_vertify']!=false}
        <i class="fa fa-vimeo {if $question['author_has_vertify'][0]=='0'}v_person {else}v_company {/if}  " data-toggle="tooltip" data-placement="right" title="" {if $question['author_has_vertify'][0]==0}data-original-title="个人认证" {else}data-original-title="企业认证" {/if} ></i>{/if}
         </span>
       
            {/if}
   
    {if $question['askuid']>0}  <span class="span_yaoqing"><i class="icon_yqhd"></i></span>  {/if}
    
  {if $question['hasvoice']!=0}<span	class=""><i class="icon_voice"></i></span> {/if}
    </div>

</div>
    <p class="qtitle"><a href="{url question/view/$question['id']}">{$question['title']}</a></p>
   
   {if count($question['images'][1])>0}  
<div class="weui-flex">
 {if count($question['images'][1])>1}
 {loop  $question['images'][1] $index $img}
  {if  $index<=2}
    <div class="weui-flex__item"><div class="imgthumbsmall"> <a href="{url question/view/$question['id']}"><img src="{$img}"></a></div></div>
    {/if}
 {/loop}
 {else}
  {if $question['image']!=null&&count($question['images'][1])<=1}
  {loop  $question['images'][1] $index $img}
   <div class="weui-flex__item"><div class="imgthumbbig"><a href="{url question/view/$question['id']}"><img src="{$img}"></a></div></div>
   {/loop}
  {/if}
 {/if}
</div>
 {/if}
<p class="description">

 <a href="{url question/view/$question['id']}">{eval echo clearhtml($question['description'],50);} </a>
</p>
    <p class="meta">
       <span>
          <i class="icon_huida"></i>{$question['answers']}
       </span>
        <span>
           <i class="icon_liulan"></i> {$question['views']}
       </span>
   
         {if $question['shangjin']!=0}  
         
         
           <span class="shangval "><i class="icon_shang"></i><span class="val">$question['shangjin']元</span></span>
           {/if}
             {if $question['price']!=0} 
           <span class="shangval jifenzhi"><i class="icon_jifen"></i><span class="val">$question['price']元</span></span>
            {/if}
    </p>
</div>
   
   
  <!--{/loop}-->
</div>
                    <!--{else}-->
                            <div id="no-result">
                <p>抱歉，未找到和您搜索相关的内容。</p>
                <strong>建议您：</strong>
                <ul class="nav">
                    <li><span>检查输入是否正确</span></li>
                    <li><span>简化查询词或尝试其他相关词</span></li>
                </ul>
            </div>
    <!--{/if}-->
    <div class="pages">  {$departstr}</div>
                    </div>


   <script>

   el2=$.tips({
        content:' 为您找到相关结果约{$rownum}个',
        stayTime:3000,
        type:"info"
    });
   </script>
<!--{template footer}-->