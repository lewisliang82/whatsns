 <div class="m_search userspaceheader">

    <div class="weui-flex attention">
      
        <div class="weui-flex__item {if $attentiontype=='question'}current{/if}"> <span class="ws_h_title"><a href="{url user/attention/question}">问题</a></span></div>
         <div class="weui-flex__item {if $attentiontype=='article'}current{/if}"> <span class="ws_h_title"><a href="{url user/attention/article}">文章</a></span></div>
         <div class="weui-flex__item {if $attentiontype==''}current{/if}"> <span class="ws_h_title"><a href="{url user/attention}">用户</a></span></div>
         <div class="weui-flex__item {if $attentiontype=='topic'}current{/if}"> <span class="ws_h_title"><a href="{url user/attention/topic}">话题</a></span></div>
    </div>
</div>